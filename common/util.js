import emoji_List from '@/common/emojiList.js';
//替换表情符号为图片

export default {
	
	replaceEmoji(content) {
		let replacedStr = content.replace(/\[([^(\]|\[)]*)\]/g, (item, index) => {
			for (let i = 0; i < emoji_List.length; i++) {
				let row = emoji_List[i];
				for (let j = 0; j < row.length; j++) {
					let EM = row[j];
					if (EM.alt == item) {
						//在线表情路径，图文混排必须使用网络路径，请上传一份表情到你的服务器后再替换此路径
						//比如你上传服务器后，你的100.gif路径为https://www.xxx.com/emoji/100.gif 则替换onlinePath填写为https://www.xxx.com/emoji/
						let onlinePath = '/static/img/emoji/';
						let imgstr = '<img src="' + onlinePath + EM.url + '">';
						return imgstr;
					}
				}
			}
		});
		return '<div class=\'text-show\'>' + replacedStr + '</div>';
	},
	
	 copySubList(list,start,length){
	    let rs = [];
	    for(let i=start;i<list.length&&i<start+length;i++){
	        rs.push(list[i]);
	    }
	    return rs;
	},
	
}
